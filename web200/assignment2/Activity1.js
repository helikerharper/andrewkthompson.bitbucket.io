// This program takes variables for hours and rate per hour and then calculate and displays weekly, monthly, and annual gross pay (hours * rate). 
// Andrew Thompson
main();

function main() {
    var hours = getHours();
    var rate = getRate();
    var week = getWeek(rate, hours);
    var year = getYear(week);
    var month = getMonth(year);
    displayResult(week, month, year)
}

function getHours() {
    var hours = prompt("How many hours do you work in a week?");
    return hours;
}

function getRate() {
    var rate = prompt("How much are you paid per hour?");
    return rate;
}

function getWeek(rate, hours) {
    var week = hours * rate;
    return week;
}

function getYear(week) {
    var year = week * 52;
    return year;
}

function getMonth(year) {
    var month = year / 12;
    return month;
}

function displayResult(week, month, year) {
    var m = month.toFixed(0);
    console.log("You Make: ");
    console.log(week + " dollars per week, " + m + " dollars per month, " + year + " dollars per year.")
}