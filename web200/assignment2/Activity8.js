//This program demonstrates string operations and string formatting
//Andrew Thompson
//by actiity 8 I mean the second activity under Data Types
main();

function main() {
    var x = "42"; // entering x with quotes creates a string variable type
    var str = "Jackie Robinson\'s number was " // \', and \" add apostrphes and qoutes to a string respectively
    var xNum = getxNum(x) //using Number()
    var concatenated = concate(str, xNum); //using concat
    var statement = addQuotes(concatenated); //adding quotes, and using replace
    document.getElementById("statement").innerHTML = statement;
}

function getxNum(x) { //Number() converts a string to a number.
    var xNum = Number(x);
    return xNum;
}

function concate(str, xNum) { //concat combines strings and/or numbers.
    var concatenated = str.concat(xNum);
    return concatenated
}

function addQuotes(concatenated) {
    var quote = "\" \""
    var statement = quote.replace(" ", concatenated); // space beween quotes replaced with our expression.
    return statement;
}