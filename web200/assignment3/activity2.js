//Create a program that asks the user how old they are in years,
// and then calculate and display their approximate age in months, days, hours, and seconds. 
//For example, a person 1 year old is 12 months old, 365 days old, etc.
//Andrew Thompson

main ();

function main() {
    var years = getYears();
    var months = calcMonths(years);
    var days = calcDays(years);
    var hours = calcHours(days);
    var seconds = calcSeconds(hours);
    displayResults(years, months, days, hours, seconds);
}

function getYears() {
    var years = prompt("How many years old are you?");
    return years;
}

function calcMonths(years) {
    var months = years * 12;
    return months;
}

function calcDays(years) {
    var days = years * 365;
    return days;
}

function calcHours(days) {
   var hours = days * 24;
   return hours;
}

function calcSeconds(hours) {
    var seconds = hours * 3600
    return seconds;
 }

 function displayResults(years, months, days, hours, seconds) {
     var ageResults = "The moment you turned: " + years  + ", You were " + months + " months " + days + " days " + hours + " hours " + " and " + seconds + " seconds old.";
    document.getElementById("ageResults").innerHTML = ageResults;
 }
