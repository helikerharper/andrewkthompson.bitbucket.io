// Create a program that uses a loop to generate a list of multiplication expressions for 
// a given value. Ask the user to enter the value and the number of expressions to be displayed.
//ref:https://www.w3schools.com/howto/howto_css_list_without_bullets.asp
//ref: https://www.w3schools.com/js/js_loop_for.asp
window.addEventListener("load", function () {
    document.getElementById("value").addEventListener("input", inputInput);
    document.getElementById("numOfExpressions").addEventListener("input", inputInput);

  });

  function inputInput() {
    let value = document.activeElement.value;
  
    if (checkInput()) {
      document.getElementById("error1").innerText = "";
      displayList();
    }
  }

  function checkInput() {
    let value = document.activeElement.value;
    if (isNaN(value) || value.trim().length == 0) {
      document.getElementById("error1").innerText = 
        document.activeElement.id + " please enter a number";
      return false;
    }
  
    value = document.getElementById("numOfExpressions").value;
    if (isNaN(value) || value.trim().length == 0) {
      return false;
    }
  
    value = document.getElementById("value").value;
    if (isNaN(value) || value.trim().length == 0) {
      return false;
    }
  
  
    return true;
  }

  function displayList() {
    let numOfExpressions = Number(document.getElementById("numOfExpressions").value);
    let value = Number(document.getElementById("value").value);
  
    if (numOfExpressions <= 0) {
      document.getElementById("error1").innerText = "Number of expressions must be at least 1"
      document.getElementById("list").innerText = "";
      return;
    }
  
  
    let result = ""
    var operand = 1;
    var answer = 1;
    for (operand = 1; operand <= numOfExpressions; operand ++) {
      answer = value * operand ;
      result += `<li> ${value} * ${operand} = ${answer}</li>`;
    }
    document.getElementById("list").innerHTML = result;
  }
