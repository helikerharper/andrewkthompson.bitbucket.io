

document.getElementById('submit').onclick = handleInput;
    
 var guessCount = 0;
 var min = 0;
 var max = 100;
 var guess = 50;

function handleInput() {
   if (checkInput()) {
        document.getElementById("error").innerText = "";
       displayResults(guessCount, min, max, guess); 
    }
}

function checkInput() {
    let highLow = document.getElementById("highLow").value;
    if (highLow.trim().length != 1 ) {
        document.getElementById("error").innerText = " You must enter h, l, or e";
        return false;
    }  

    return true;
}

function displayResults() {
    guessCount = Number(guessCount);
    guessCount++
    let highLow = document.getElementById("highLow").value;
    
    if (highLow == "h" || highLow == "H") {
        max = Number(guess) - 1;
    }

    if (highLow == "l" || highLow == "L") {
        min = Number(guess) + 1;
    }
    if (highLow == "e" || highLow == "E") {
        document.getElementById("guessCount").innerText = "It took me " + guessCount + " tries"
     }

   guess = Math.round((min + max) / 2);
   let guessResult = "";
   guessResult = guess.toString();
 document.getElementById("guess").innerText = guessResult;
}

